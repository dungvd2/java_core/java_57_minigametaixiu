package Main;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

/* Một người chơi sẽ có tài khoản. Người chơi có quyền đặt cược số tiền ít hơn
 * hoặc bằng số tiền họ đang có.
 * Luật chơi như sau:
 * Có 3 viên xúc xắc. Mỗi viên xúc xắc có 6 mặt có giá trị từ 1 đến 6.
 * Mỗi lần lắc sẽ ra một kết quả. Kết quả = giá trị xx1 + giá trị xx2 + giá trị xx3
 * 1. Nếu tổng  = 3 hoặc 18 => Nhà cái ăn hết, người chơi thua
 * 2. Nếu tổng = 4-10 xỉu 11-17 tài
 * 
 */
public class TaiXiu {
	public static void main(String[] args) {
		double taiKhoanNguoiChoi = 5000;
		Scanner sc = new Scanner(System.in);
		// Hàm format kiểu số
		Locale lc = new Locale("vi", "VN"); // lấy ra vị trị
		NumberFormat numf = NumberFormat.getCurrencyInstance(lc);
		
		int luaChon = 1;
		do {
			System.out.println("------MỜI BẠN LỰA CHỌN-------");
			System.out.println("Chọn (1) để tiếp tục chơi.");
			System.out.println("Chọn (phím bất kỳ) để thoát.");
			luaChon = sc.nextInt();
			if(luaChon == 1) {
				System.out.println("*** BẮT ĐẦU CHƠI: ");
				System.out.println("Tài khoản của bạn: " + numf.format(taiKhoanNguoiChoi)  + " , bạn muốn cược bao nhiêu?");
				double datCuoc = 0;
				do {
					System.out.println("** Đặt cược không quá " + numf.format(taiKhoanNguoiChoi) + "***");
					datCuoc = sc.nextDouble();
				} while(datCuoc <= 0 || datCuoc > taiKhoanNguoiChoi);
				
				int luaChonTaiXiu = 0;
				do {
					System.out.println("*** Chọn 1: Tài");
					System.out.println("*** Chọn 2: Xỉu");
					luaChonTaiXiu = sc.nextInt();
				} while(luaChonTaiXiu!= 1 && luaChonTaiXiu != 2);
				
				// Tung xúc xắc
				Random xucXac1 = new Random();
				Random xucXac2 = new Random();
				Random xucXac3 = new Random();
				
				int giaTri1 = xucXac1.nextInt(5) + 1;
				int giaTri2 = xucXac2.nextInt(5) + 1;
				int giaTri3 = xucXac3.nextInt(5) + 1;
				int tong = giaTri1 + giaTri2 + giaTri3;
				// Tính toán kết quả
				System.out.println("Kết quả: " + giaTri1 + "-" + giaTri2 + "-" + giaTri3 );
				System.out.println(tong);
				if(tong == 3 || tong == 18) {
					taiKhoanNguoiChoi -= datCuoc;
					System.out.println("*** Tổng điểm = " + tong + " => Nhà cái thắng");
					System.out.println("*** Tài khoản của bạn là: " + numf.format(taiKhoanNguoiChoi));
				}
				else if(tong >= 4 && tong <= 10) {
					if(luaChonTaiXiu == 2) {
						taiKhoanNguoiChoi += datCuoc;
						System.out.println("*** Tổng điểm = " + tong + " Xỉu => Bạn thắng");
						System.out.println("*** Tài khoản của bạn là: " + numf.format(taiKhoanNguoiChoi));
					}
					else {
						taiKhoanNguoiChoi -= datCuoc;
						System.out.println("*** Tổng điểm = " + tong + " Xỉu => Nhà cái thắng");
						System.out.println("*** Tài khoản của bạn là: " + numf.format(taiKhoanNguoiChoi));
					}
				}
				else if(tong >= 11 && tong <= 17) {
					if(luaChonTaiXiu == 1) {
						taiKhoanNguoiChoi += datCuoc;
						System.out.println("*** Tổng điểm = " + tong + " Tài => Bạn thắng");
						System.out.println("*** Tài khoản của bạn là: " + numf.format(taiKhoanNguoiChoi));
					}
					else {
						taiKhoanNguoiChoi -= datCuoc;
						System.out.println("*** Tổng điểm = " + tong + " Tài => Nhà cái thắng");
						System.out.println("*** Tài khoản của bạn là: " + numf.format(taiKhoanNguoiChoi));
					}
				}
			}
		} while(luaChon==1) ;
	}
	
}
